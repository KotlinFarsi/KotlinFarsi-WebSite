---
layout: tutorial
title: "فهرست"
category: js
permalink: /tutorials/js/
editlink: https://github.com/KotlinFarsi/OpenSourceTutorials-Browser/edit/master/src/README.md
---

<div dir="rtl" markdown="1">

# فهرست

**1 - مقدمه**

[1.1 ) یک Hello World ساده](./hello-world/README.md)

**2 - مبانی کاتلین/JS**

[2.1 ) کار با DOM - بخش اول](./working-with-the-dom-part1/README.md)

[2.2 ) کار با DOM - بخش دوم](./working-with-the-dom-part2/README.md)

[2.3 ) کار با DOM - بخش سوم](./working-with-the-dom-part1/README.md)

**2 - همکاری با JS**

[2.1 ) همکاری با JS](./javascript-introp/README.md)

بقیه فصل ها در حال ساخت است
